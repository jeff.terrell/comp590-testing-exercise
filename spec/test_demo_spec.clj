(ns test-demo-spec
  (:require [speclj.core :refer [describe it should should=]]
            [test-demo :as sut]))

(describe "summation"
  (it "returns 0 given an empty sequence")
  (it "maps the given fn arg over the sequence before summing")
  (it "sums the items of the mapped sequence"))
