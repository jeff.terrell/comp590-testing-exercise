# [UNC COMP 590-145](https://comp590-s20.cs.unc.edu/) testing exercise

Given in class on Wednesday, February 12, 2020.

### Initial setup

In a bash-compatible terminal:

```
mkdir classes
clojure -Sdeps '{:deps {speclj
                      {:git/url "https://github.com/kyptin/speclj"
                       :sha "a843b64cc5a015b8484627eff6e84bbac2712692"}}}' \
        -e "(do (compile 'speclj.platform.SpecFailure)
                (compile 'speclj.platform.SpecPending))"
```

### Usage

    clojure -A:test -a -c

