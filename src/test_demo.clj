(ns test-demo)

(defn summation
  ([xs] (summation identity xs))
  ([f xs] (reduce + 0 (map f xs))))
